#ifndef COMMON_H
#define COMMON_H
#include <stdbool.h>

float *allocate_square_matrix(unsigned int n);
void rand_init_square_matrix(float *m, unsigned int n);
bool get_arguments(int argc, char **argv, unsigned int *threads, unsigned int *n);
void matrix_mul_row(float *op1, float *op2, float *res, unsigned int i, unsigned int n);
bool is_power_of_two(unsigned int x);

#endif