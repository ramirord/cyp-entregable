#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "../common.h"

float *a;
float *b;
float *c;

struct worker_args {
	unsigned id;
	unsigned first_row;
	unsigned rows;
	float *a;
	float *b;
	float *c;
};

static void *worker(void *args)
{
	struct worker_args* vars = (struct worker_args*) args;
	printf("Worker %u processing rows starting from %u to %u.\n",
		vars->id,
		vars->first_row,
		vars->first_row + vars->rows - 1);
	for (int row = 0; row < vars->rows; row++)
		matrix_mul_row( a,
				b,
				c,
				vars->first_row + vars->rows,
				vars->rows);

	printf("Worker %i done.\n", vars->id);
	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	int thread_count; 
	int dimension;
	srand (time(NULL));

	if (!get_arguments(argc, argv, &thread_count, &dimension)) {
		printf("main <threads> <N>\n");
		fflush(stdout);
		return -1;
	}
	printf("T = %u\n", thread_count);
	printf("N = %u\n", dimension);

	a = allocate_square_matrix(dimension);
	b = allocate_square_matrix(dimension);
	c = allocate_square_matrix(dimension);
	rand_init_square_matrix(a, dimension);
	rand_init_square_matrix(b, dimension);

	pthread_t threads[thread_count];
	struct worker_args args[thread_count];
	for (int i = 0; i < thread_count; i++) {
		args[i].id = i;
                args[i].rows = dimension / thread_count;
                args[i].first_row = i * (dimension / thread_count);
		args[i].a = a;
		args[i].b = b;
		args[i].c = c;
		pthread_create(threads + i, NULL, worker, args + i);
	}

	for (int i = 0; i < thread_count; i++)
		pthread_join(threads[i], NULL);
}