#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>
#include "../common.h"

static void master_work();
static void slave_work();

int main(int argc, char **argv)
{
	int id;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &id);

	if (id == 0)
		master_work();
	else
		slave_work();

	MPI_Finalize();
}

void master_work()
{
	unsigned int dimension;
	
	do {
		char line[10];
		printf("Ingrese dimension de matriz.\nDebe ser potencia de dos: ");
		fflush(stdout);
		fgets(line, 10, stdin);
		sscanf(line, "%u\n", &dimension);
	} while (!is_power_of_two(dimension));

	printf("Generando matrices de %ux%u ...\n", dimension, dimension);
	float (*a)[dimension] = (float (*)[dimension]) allocate_square_matrix(dimension);
	float (*b)[dimension] = (float (*)[dimension]) allocate_square_matrix(dimension);
	float (*c)[dimension] = (float (*)[dimension]) allocate_square_matrix(dimension);

	srand (time(NULL));
	rand_init_square_matrix((float*) a, dimension);
	rand_init_square_matrix((float*) b, dimension);
	puts("Matriz generada.");

	puts("Transfiriendo a esclavos...");
	int process_count;
	MPI_Comm_size(MPI_COMM_WORLD, &process_count);
	/* Mandar dimension */
	for (int i=1; i < process_count; i++)
		MPI_Send(&dimension, 1, MPI_UNSIGNED, i, 0, MPI_COMM_WORLD);
	/* Mandar matrices */
	for (int i=1; i < process_count; i++) {
		printf("Mandando matrices a %u.\n", i);
		MPI_Send(a, dimension * dimension, MPI_FLOAT, i, 0, MPI_COMM_WORLD);
		MPI_Send(b, dimension * dimension, MPI_FLOAT, i, 0, MPI_COMM_WORLD);
	}
	puts("Matrices transferidas.");

	
	const unsigned int slice_size = dimension / (unsigned int) process_count;

	puts("Multiplicando primera porción ...");
	/* Multiplicar la primera porción */
	for (unsigned int i = 0; i < slice_size; i++) {
		for (unsigned int j = 0; j < dimension; j++) {
			c[i][j] = 0;
			for (unsigned int k = 0; k < dimension; k++)
				c[i][j] += a[i][k] * b[k][j];
		}
	}

	puts("Esperando resultados ...");
	/* Recibir resultados fuera de orden */
	for (int i=1; i < process_count; i++) {
		/* Recibir número de fila */
		MPI_Status status;
		MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		const unsigned offset = status.MPI_SOURCE * slice_size;
		MPI_Recv(c + offset, slice_size * dimension, MPI_FLOAT, status.MPI_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("Maestro recibió %u filas del proceso %u.\n", slice_size, status.MPI_SOURCE);
	}
	
	puts("Fin de multiplicación");
	free(a);
	free(b);
}

void slave_work()
{
	int id, process_count;
	unsigned int dimension;

	MPI_Comm_rank(MPI_COMM_WORLD, &id);
	MPI_Comm_size(MPI_COMM_WORLD, &process_count);


	MPI_Recv(&dimension, 1, MPI_UNSIGNED, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	const unsigned int slice_size = dimension / (unsigned int) process_count;

	float (*a)[dimension] = (float (*)[dimension]) allocate_square_matrix(dimension);
	float (*b)[dimension] = (float (*)[dimension]) allocate_square_matrix(dimension);
	float (*rows)[dimension] = (float (*)[dimension]) malloc(slice_size * dimension * sizeof(float));
	
	MPI_Recv(a, dimension * dimension, MPI_FLOAT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	MPI_Recv(b, dimension * dimension, MPI_FLOAT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	
	const unsigned int row_no = id * slice_size;
	for (unsigned int i = 0; i < slice_size; i++) {
		for (unsigned int j = 0; j < dimension; j++) {
			rows[i][j] = 0;
			for (unsigned int k = 0; k < dimension; k++)
				rows[i][j] += a[row_no + i][k] * b[k][j];
		}
	}
	MPI_Send(rows, dimension * slice_size, MPI_FLOAT, 0, 0, MPI_COMM_WORLD);
	
	free(a);
	free(b);
	free(rows);
}