with Ada.Text_IO;
with Ada.Float_Text_IO;
with Ada.Numerics.Float_Random;

use Ada; 

procedure Uno is
subtype Temperatura is float range -273.14 .. 200.0;

-- Esperanza de la temperatura
Mu: constant Temperatura := 25.0;

-- Desviación estándar de la temperatura
Sigma: constant Temperatura := 5.0;

-- Escala del tiempo
Alfa: constant Standard.Duration := 0.2;

task Timer is
	entry Iniciar;
end;

task Central is
	entry Recibir_Uno(T: in Temperatura);
	entry Recibir_Dos(T: in Temperatura);
	entry Timer_Vencido;
end;

task Sensor_Uno;
task Sensor_Dos;

task body Timer is
begin
	loop
		accept Iniciar;
		delay(3*60.0*Alfa);
		Central.Timer_Vencido;
	end loop;
end;

procedure Imprimir_Temp(T: in Temperatura) is
begin
	Float_Text_IO.Put(T);
	Text_IO.New_Line;
	Delay(1.0);
end;

procedure Imprimir_Uno(T: in Temperatura) is
begin
	Text_IO.Put("Sensor 1:");
	Imprimir_Temp(T);
end;

procedure Imprimir_Dos(T: in Temperatura) is
begin
	Text_IO.Put("Sensor 2:");
	Imprimir_Temp(T);
end;

task body Central is
Solo_Dos: Boolean;
begin
	accept Recibir_Uno(T: in Temperatura) do
		Imprimir_Uno(T);
	end;

	loop
		select
			accept Recibir_Uno(T: in Temperatura) do
				Imprimir_Uno(T);
			end;
		or
			accept Recibir_Dos(T: in Temperatura) do
				Imprimir_Dos(T);
			end;
			Timer.Iniciar;
			Solo_Dos := True;
			while Solo_Dos loop
				select
					accept Recibir_Dos(T: in Temperatura) do
						Imprimir_Dos(T);
					end;
				or
					accept Timer_Vencido do
						Solo_Dos := False;
					end;
				end select;
			end loop;
		end select;
	end loop;
end;

task body Sensor_Uno is
T: Temperatura;
Gen: Numerics.Float_Random.Generator;
begin
	Numerics.Float_Random.Reset(Gen);
	loop 
		T:= Mu + Sigma * Numerics.Float_Random.Random(Gen);
		select 
			Central.Recibir_Uno(T);
		or
			delay(2.0*60.0*Alfa);
		end select;
	end loop;
end;

task body Sensor_Dos is
T: Temperatura;
Gen: Numerics.Float_Random.Generator;
begin
	Numerics.Float_Random.Reset(Gen);
	loop
		T:= Mu + Sigma * Numerics.Float_Random.Random(Gen);
		select
			Central.Recibir_Dos(T);
		else
			delay(1*60.0*Alfa);
			Central.Recibir_Dos(T);
		end select;
	end loop;
end;

begin
	null;
end;