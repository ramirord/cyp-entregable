#include "common.h"
#include <limits.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

float *allocate_square_matrix(unsigned int n) 
{
	/*if (n >= (unsigned int) sqrt((float) UINT_MAX))
		return malloc(n*n * sizeof(float));
	else
		return NULL;*/
	return malloc(n*n * sizeof(float));
}

void rand_init_square_matrix(float *m, unsigned int n)
{
	for (unsigned int i = 0; i < n * n; i++)
		m[i] = rand() * 100;
}

bool is_power_of_two(unsigned int x)
{
	return (x != 0) && ((x & (x - 1)) == 0);
}

bool get_arguments(int argc, char **argv, unsigned int *threads, unsigned int *n) {
	if (argc != 3)
		return false;
	*n = strtol(argv[2], NULL, 10);
	*threads = strtol(argv[1], NULL, 10);
	return is_power_of_two(*n) && is_power_of_two(*threads);
}

void matrix_mul_row(float *op1, float *op2, float *res, unsigned int i, unsigned int n)
{
	float (*a)[n] = (float (*)[n]) op1;
	float (*b)[n] = (float (*)[n]) op2;
	float (*c)[n] = (float (*)[n]) res;
	for (unsigned int j = 0; j < n; j++) {
		c[i][j] = 0;
		for (unsigned int k = 0; k < n; k++)
			c[i][j] += a[i][k] * b[k][j];
	}
}
