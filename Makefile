CC = cc -g -Wall
MPICC = mpicc -g -Wall
INCLUDE = .

all: mc/main pm/main

mc/main: mc/main.o common.o
	$(CC) -o mc/main mc/main.o common.o -lpthread

pm/main: pm/main.o common.o
	$(MPICC) -o pm/main pm/main.o common.o 

mc/main.o: mc/main.c common.h
	$(CC) -c -o mc/main.o mc/main.c

common.o: common.h common.c
	$(CC) -c -o common.o common.c

pm/main.o: pm/main.c common.h
	$(MPICC) -c -o pm/main.o pm/main.c

clean:
	rm -rvf *.o mc/main pm/main
